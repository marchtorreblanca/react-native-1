// import a Library to help creat a Component
import React from 'react';
import { Text, AppRegistry, } from 'react-native';

// Create a Component
const App = () => {
  return (
    <Text>Some Text </Text>
  );
};

//Render it to the device
AppRegistry.registerComponent('AwesomeProject2', () => App);
